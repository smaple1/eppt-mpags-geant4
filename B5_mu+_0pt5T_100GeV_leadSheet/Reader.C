#define Reader_cxx
#include "Reader.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <algorithm>

TRandom3 *rndm=new TRandom3(9);

void Reader::Loop()
{
   if (fChain == 0) return;

   std::vector<double> errorX = {0.1, 0.1, 0.1, 0.1, 0.1};
   std::vector<double> errorZ = {0, 0, 0, 0, 0};

   TH1D *RecP_T_over_P_T = new TH1D("RecP_T_over_P_T", "pt_rec/pt", 50, 0, -1);

   Long64_t nentries = fChain->GetEntriesFast();
   for (Long64_t jentry {0}; jentry < nentries; jentry++) {
     GetEntry(jentry);
       // smear the x positions of each drift chamber measurement
       std::transform(Dc1HitsVector_x->begin(), Dc1HitsVector_x->end(), Dc1HitsVector_x->begin(), [&](double xVal){ return rndm->Gaus(xVal, 0.1); });
       std::transform(Dc2HitsVector_x->begin(), Dc2HitsVector_x->end(), Dc2HitsVector_x->begin(), [&](double xVal){ return rndm->Gaus(xVal, 0.1); });

       TGraphErrors *g1 = new TGraphErrors(5, &(Dc1HitsVector_z->at(0)), &(Dc1HitsVector_x->at(0)), &errorZ[0], &errorX[0]);
       g1->Fit("pol1");

       TGraphErrors *g2 = new TGraphErrors(5, &(Dc2HitsVector_z->at(0)), &(Dc2HitsVector_x->at(0)), &errorZ[0], &errorX[0]);
       g2->Fit("pol1");
       
       // multiply the gradient as layer spacing 500mm yet z=1,2,3?
       double grad1 = (g1->GetFunction("pol1")->GetParameter(1))/500;
       double grad2 = (g2->GetFunction("pol1")->GetParameter(1))/500;
       double theta = TMath::ATan(grad2)-TMath::ATan(grad1);
       double recP_T= TMath::Sqrt(TMath::Power((0.3 * 0.5) / ( TMath::Sin(theta/2) ), 2));
       RecP_T_over_P_T->Fill(recP_T/100);

       // auto c = new TCanvas();
       // c->Divide(2,1);
       // c->cd(1);
       // g1->SetTitle("Dc1 Reconstructed Track");
       // g1->GetXaxis()->SetTitle("layer number");
       // g1->GetYaxis()->SetTitle("x position/mm");
       // g1->Draw();
       // c->cd(2);
       // g2->SetTitle("Dc2 Reconstructed Track");
       // g2->GetXaxis()->SetTitle("layer number");
       // g2->GetYaxis()->SetTitle("x position/mm");
       // g2->Draw();
       // }
       // if (jentry > 0) {
       // break;
       // }
   }
   RecP_T_over_P_T->Draw("hist");

}
